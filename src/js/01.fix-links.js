//
// Patch Product Listing Links
//---------------------------------
$(document).ajaxComplete(function(){
    $('.prod_list_image_outer > a, .x-form-action > a').each(function(){
        let uri = $(this).attr('href').replace('www.brand-estore.com', 'localhost:3000');
        $(this).attr('href', uri);
    });
});

//
// Patch Login Component
//----------------------------------
// $(function(){
//     if (window.location.pathname.match(/SignIn/i)){
//         $('.maincontainer script:last-child').remove();
//     }
// });