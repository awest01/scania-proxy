'use strict'
var gulp         = require('gulp'),
    browserSync  = require('browser-sync').create(),
    concat       = require('gulp-concat'),
    fs           = require('fs'),
    mcache       = require('memory-cache'),
    url          = require('url'),
    md5          = require('md5'),
    path         = require('path'),
    zlib         = require("zlib"),
    sass         = require('gulp-sass'),
    postcss      = require('gulp-postcss'),
    autoprefixer = require('autoprefixer'),
    jsdom        = require('jsdom'),
    async        = require('async');

const { JSDOM } = jsdom;

var paths = {
    cache:   "./dist/.cache",
    scripts: "./src/js/**/*.js",
    styles:  "./src/css/**/*.scss",
    outdir:  "./dist/local"
};

// Cache TTL Setting 
const duration = 60000 * 24;

//
// File & Memory Cache
//-------------------------------------
const postprocess = (url, buffer) => {
    let html   = buffer.toString();
    let dom    = new JSDOM(html);

    let headerCache = cache.get("detached_header", "transient");

    if (headerCache){
        let header = dom.window.document.querySelector("#header");

        if (header != null){
            header.innerHTML = headerCache;
        }
    } else {
        let ele = dom.window.document.querySelector("#header")

        if (ele != null){
            let html = ele.innerHTML;
            console.log("TRANSIENT - PUT: header")
            cache.put("detached_header", '<!--' + url + '-->' + html, "transient");
        }
    }

    if (url.match(/signin/i)){
        dom.window.document.querySelector('.maincontainer script:last-child').remove();
    }

    // Clean up css/js
    // let elements = dom.window.document.querySelectorAll('link');

    // elements.forEach((el) => {
    //     el.remove();
    // })

    // dom.window.document.querySelector('head').innerHTML += '<link href="/local/bootstrap-bundle.css" rel="stylesheet" type="text/css" data-keep="true" />';

    return Buffer.from(dom.serialize());
}

const cache = {
    last: {
        key: "",
        dir: "",
    },
    get: (key, dir = "page") => {
        let content       = mcache.get(key);
        let filecacheKey  = md5(key);
        let filecacheDir =  path.join(paths.cache, dir);
        let filecacheName = path.join(filecacheDir, filecacheKey);

        if (content == null && fs.existsSync(filecacheName)){
            let stat = fs.statSync(filecacheName);
            let seconds = (new Date().getTime() - stat.mtime);

            if (seconds < duration) {
                console.log("READING FROM FILE: " + filecacheName);
                content = fs.readFileSync(filecacheName);

                cache.put(key, content, dir);
            } else {
                cache.flush("transient");
            }
        }

        cache.last.key = filecacheKey;
        cache.last.dir = dir;

        return content;
    },
    put: (key, data, dir = "page") => {
        let filecacheKey  = md5(key);
        let filecacheDir  = path.join(paths.cache, dir);
        let filecacheName = path.join(filecacheDir, filecacheKey);

        if (!fs.existsSync(filecacheDir)){
            fs.mkdirSync(filecacheDir, { recursive: true });
        }

        mcache.put(key, data, duration);

        fs.writeFileSync(filecacheName, data, {
            flag: 'w+'
        });

        cache.last.key = filecacheKey;
        cache.last.dir = dir;
    },
    flushlast: () => {
        console.log("FLUSH LAST PAGE: " + cache.last.key + '::' + cache.last.dir);

        mcache.clear();

        if (cache.last.key){
            try {
                fs.unlinkSync(path.join(paths.cache, cache.last.dir, cache.last.key));
            } catch(ex){

            }

            cache.flush("transient");
        }
    },
    flush: (dir = "page", key = null) => {
        var cachedir = path.join(paths.cache, dir);

        mcache.clear();

        fs.readdir(cachedir, (err, files) => {
            if (!err){
                for (const file of files) {
                    try {
                        fs.unlinkSync(path.join(cachedir, file));
                    } catch (ex){
                        console.log(ex.message);
                    }
                }
            }
        });
    },
    clean: () => {
        var cachedir = path.join(paths.cache, 'page');

        try {
            fs.unlinkSync(path.join(cachedir, 'a23ea3951a17b3078793b2a947afbdaa'))
        } catch (ex){
            console.log(ex.message);
        }

        cache.flush('transient');
    }
}

//
// Gulp Tasks
//---------------------------------
function buildScripts(){
    return gulp.src(paths.scripts)
        .pipe(concat('custom.js'))
        .pipe(gulp.dest(paths.outdir));
}

function buildStyles(){
    return gulp.src(paths.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(postcss([autoprefixer()]))
        .pipe(gulp.dest(paths.outdir))
}

function bundleCSS(){
    return gulp.src("./dist/scania/css/bootstrap*.css")
        .pipe(concat('bootstrap-bundle.css'))
        .pipe(gulp.dest("./dist/local/"));
}

const buildSeries = gulp.series(buildScripts, buildStyles);

function watchTask(){
    console.log("Cleaning Cache");
    cache.clean();

    browserSync.init({
        proxy: 'https://www.brand-estore.com/scania',
        serveStatic: [
            {
                route: '/local',
                dir: './dist/local',
            },
            {
                route: '/scania',
                dir: './dist/scania'
            }
        ],
        middleware: [
            // (req, res, next) => {
            // },
            //
            // Handle Static Files
            //-----------------------------
            (req, res, next) => {
                let urlObj = url.parse(req.url);
                let filename = path.basename(urlObj.pathname).toLowerCase();
                let dirname  = ['./dist/', path.dirname(urlObj.pathname)].join('/').toLowerCase();

                if (filename.match(/(js|css|jpg|png|woff|woff2|html)$/i)){
                    if (!fs.existsSync(dirname)){
                        fs.mkdirSync(dirname, { recursive: true });
                    }
                    
                    var chunks = [];

                    res.sendResponse = res.write;
                    res.sendEnd = res.end;

                    res.write = (chunk) => {
                        chunks.push(chunk);
                        res.sendResponse(chunk);
                    };

                    res.end = () => {
                        let buffer =  Buffer.concat(chunks);
                        let saveAs = dirname + '/' + filename;

                        if (buffer.length != 1245) {
                            console.log("STATIC CACHE - PUT: " + filename);

                            if (filename.match(/(js|css)$/i)){
                                
                                zlib.gunzip(buffer, function(err, buffer){
                                    if (!err){
                                        fs.appendFileSync(saveAs, buffer, {
                                            flag: 'w'
                                        });
                                    }
                                });
                            } else {
                                fs.writeFileSync(saveAs, buffer);
                            }
                        }

                        res.sendEnd();
                    }
                }

                next();
            },
            //
            // Handle Page Caching
            //-------------------------
            (req, res, next) => {
                let key  = req.url.replace(/\//g, '');

                if (req.headers['accept-encoding'] == 'identity' && !req.url.match(/\/scania\/$/i) && req.method === 'GET'){
                    key = md5(key);
                    let dir = "page";

                    if (req.url.match(/checkout/i)){
                        dir = "transient";
                        cache.flush('transient');
                    }

                    if (req.url.match(/(signin|checkout|signout)/i)){
                        cache.flush('transient');
                        cache.flushlast();
                    }

                    let body = cache.get(key, dir);

                    if (body){
                        console.log("PAGE CACHE - GET: " + req.url);

                        res.write(postprocess(req.url, body));
                        res.end();

                        return;
                    } else {
                        var chunks = [];

                        res.sendResponse = res.write;
                        res.sendEnd = res.end;

                        res.write = (chunk) => {
                            chunks.push(chunk);
                        };

                        res.end = () => {
                            console.log("PAGE CACHE - PUT: " + req.url);

                            let buffer = postprocess(req.url, Buffer.concat(chunks));

                            res.sendResponse(buffer);

                            cache.put(key, buffer, dir);
                            res.sendEnd();
                        }
                    }
                }

                // Clear cache on all posts (we ignore files ending with JSON/ASPX -- they are ajax calls)
                if (req.method === 'POST' && !req.url.match(/(json|aspx|Signout)$/i)){
                    console.log("PAGE CACHE - FLUSH");

                    cache.flushlast();
                }

                next();
            }
        ],
        snippetOptions: {
            rule: {
                match: /<\/head>/i,
                fn: function(snippet, match){
                    return `
                    
                    <link href="/local/custom.css" rel="stylesheet" type="text/css" />
                    <script src="/local/custom.js" type="text/javascript" ></script>
                    ` + snippet + match;
                }
            }
        },
    });

    
    buildSeries();

    gulp.watch(paths.styles,  buildStyles);
    gulp.watch(paths.scripts, buildScripts);
    gulp.watch("./dist/scania/css/*.css", bundleCSS);
}

exports.build = buildSeries;
exports.watch = watchTask;